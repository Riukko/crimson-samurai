﻿Shader "Unity3dTips/GrayscaleTransparent" {
    Properties{
      _MainTex("Texture", 2D) = "white" {}
      _Color("Color", Color) = (1,1,1,1)
      _Alpha("GrayscaleAlpha", Range(0.0,1.0)) = 0.0
    }
        SubShader{
          GrabPass { "_BackgroundTexture" }
          Blend SrcAlpha OneMinusSrcAlpha
          Pass {
            Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
            ZWrite Off
            ZTest Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            sampler2D _BackgroundTexture;
            sampler2D _MainTex;
            float _Alpha;
            fixed4 _MainTex_ST;
            struct v2f {
              fixed4 vertex : SV_POSITION;
              fixed4 grabUV : TEXCOORD1;
            };
            struct appdata {
              fixed4 vertex : POSITION;
            };
            v2f vert(appdata v) {
              v2f o;
              o.vertex = UnityObjectToClipPos(v.vertex);
              o.grabUV = ComputeGrabScreenPos(o.vertex);
              return o;
            }
            fixed4 frag(v2f i) : SV_Target{
            fixed4 bgc = tex2Dproj(_BackgroundTexture, i.grabUV);
            fixed4 fragColor;
            fragColor.rgb = dot(bgc.rgb, fixed3(.222, .707, .071));
            fragColor.a = bgc.a * _Alpha;
            //return (bgc.r + bgc.g + bgc.b) / 3;
            return fragColor;
            }
            ENDCG
          }
      }
          FallBack Off
}