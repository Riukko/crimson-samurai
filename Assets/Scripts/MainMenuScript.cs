﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject commandsMenu;
    public GameObject creditsMenu;
    public GameObject loreMenu;
    public GameObject particles;
    public bool goPlay = false;

    void Update()
    {
        if (goPlay && Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("Riukko's Scene");
        }
        Time.timeScale = 1f;
    }

    public void Play()
    {
        goPlay = true;
        mainMenu.SetActive(false);
        particles.SetActive(false);
        loreMenu.SetActive(true);
    }

    public void Commands()
    {
        mainMenu.SetActive(false);
        commandsMenu.SetActive(true);
        commandsMenu.transform.Find("BoutonBack").GetComponent<Button>().Select();
    }

    public void Credits()
    {
        mainMenu.SetActive(false);
        creditsMenu.SetActive(true);
        creditsMenu.transform.Find("BoutonBack").GetComponent<Button>().Select();
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Back()
    {
        commandsMenu.SetActive(false);
        creditsMenu.SetActive(false);
        mainMenu.SetActive(true);
        mainMenu.transform.Find("BoutonPlay").GetComponent<Button>().Select();
    }
}
