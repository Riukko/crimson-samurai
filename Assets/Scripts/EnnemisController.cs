﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class EnnemisController : MonoBehaviour
{
    public float movementSpeed = 1f;
    public float attackSpeed = 1f;
    public float attackDistance = 2f;
    public bool isAgressive = false;
    private Vector2 directionMouvement;
    private Rigidbody2D rb;
    private SpriteRenderer spriteRend;
    private GameObject player;
    private bool m_FacingRight = false;
    bool isAttacking = false;

    public GameObject attackBox;
    public GameObject feet;
    public GameObject target;

    Vector2 directionJoueur;

    bool playerBlocked = false;
    bool attackCollidesWithPlayer = false;

    public enum Etat
    {
        deplacement,
        attaquer,
        traitement,
        freeze,
        repousse
    }

    public Etat etatActuel;

    void Start()
    {
        GameManager.Instance.ennemyList.Add(gameObject);
        rb = this.GetComponent<Rigidbody2D>();
        spriteRend = this.GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player");
        etatActuel = Etat.deplacement;
        directionMouvement = new Vector2(0, 0);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Si on est proche du player et que l'état n'est pas freeze, on devient agressif et on cherche à se rapprocher pour l'attaquer
        if (other.tag == "Player") {
            if (etatActuel != Etat.freeze)
            {
                if (GameManager.getNbEnnemisAgressifs() < GameManager.getMaxEnnemisAgressifs())
                {
                    isAgressive = true;
                }
            }
        //Si l'ennemi est freeze et qu'on est donc en berserk mode, le trigger déclenche la mort de l'ennemi
            else
            {
                GameManager.Instance.killedEnemyList.Add(gameObject);
                
                target.SetActive(true);
            }
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if(isAgressive)
            {
                isAgressive = false;
            }
        }
    }


    private void Update()
    {
        Vector2 directionJoueur = player.transform.position - transform.position;

        if (PlayerController.Instance.isBerserk)
        {
            etatActuel = Etat.freeze;
        }
        else
        {
            checkIfWall();
            //Change l'ordre du sprite sur le layer si l'ennemi est au-dessus ou en-dessous du personnage
            if (directionJoueur.y > 0)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z - 0.5f);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z + 0.5f);
            }

            //Si l'ennemi est à portée de donner un coup on déclenche l'état attaquer
            //Sinon on reste en état déplacement
            if (Mathf.Abs(directionJoueur.x) < attackDistance && Mathf.Abs(directionJoueur.y) < 0.7 && etatActuel != Etat.attaquer && etatActuel != Etat.freeze && etatActuel != Etat.repousse)
            {
                //Si l'ennemi ne regarde pas le joueur au moment d'attaquer, on le retourne avant de le faire
                if (m_FacingRight && directionJoueur.x < 0)
                {
                    Flip();
                    etatActuel = Etat.attaquer;
                }
                else if (!m_FacingRight && directionJoueur.x > 0)
                {
                    Flip();
                    etatActuel = Etat.attaquer;
                }
                else
                {
                    etatActuel = Etat.attaquer;
                }
            }
        }
    }


    void FixedUpdate()
    {

        directionJoueur = player.transform.position - transform.position;
        
        
        //l'état d'un ennemi détermine son comportement
        switch (etatActuel)
        {
            case Etat.deplacement:
                Deplacement(directionJoueur);
                moveEnnemi(directionMouvement);
                break;
            case Etat.attaquer:
                //L'ennemi est en train d'attaquer
                if (!isAttacking)
                {
                    Attaquer(directionJoueur);
                }
                isAttacking = true;
                break;
            case Etat.repousse:
                Repousse(directionJoueur);
                break;
            case Etat.traitement:
                moveEnnemi(directionMouvement);
                break;
            case Etat.freeze:
                Freeze();
                break;
            default:
                break;
        }
        

    }

    //On regarde constamment si les ennemis ne vont pas dépasser les murs
    void checkIfWall()
    {

        if(Physics2D.Raycast((Vector2)feet.transform.position, Vector2.down, 0.5f, LayerMask.GetMask("Walls")) && directionMouvement.y < 0)
        {
            directionMouvement.y = 0;
        }
        else if(Physics2D.Raycast(feet.transform.position, Vector2.up, 0.5f, LayerMask.GetMask("Walls")) && directionMouvement.y > 0)
        {
            directionMouvement.y = 0;
        }
        else if (Physics2D.Raycast(feet.transform.position, Vector2.right, 0.5f, LayerMask.GetMask("Walls")) && directionMouvement.x > 0)
        {
            directionMouvement.x = 0;
        }
        else if (Physics2D.Raycast(feet.transform.position, Vector2.left, 0.5f, LayerMask.GetMask("Walls")) && directionMouvement.x < 0)
        {
            directionMouvement.x = 0;
        }
    }

    void Deplacement(Vector2 directionJoueur)
    {
        //L'ennemi se déplace de différentes façons en fonction du joueur et des autres ennemis
        float chancesPoursuite = 0;
        float chancesRoder = 0;

        //Si on est aggressif, on a 70% de chances d'aller poursuivre le joueur
        if(isAgressive)
        {
            chancesPoursuite = 70;
            chancesRoder = 20;
        }
        //Si x ennemis sont déjà aggressif et pas nous, on ne poursuit pas, sinon 30% de chances de poursuivre
        else if (GameManager.getNbEnnemisAgressifs() >= GameManager.getMaxEnnemisAgressifs() && !isAgressive)
        {
            chancesRoder = 80;
        }
        else
        {
            chancesPoursuite = 30;
            chancesRoder = 50;
        }
        //Le pourcentage manquant représente la chance d'attendre

        float rand = Random.Range(0, 100);
        if(rand < chancesPoursuite)
        {
            directionJoueur.Normalize();
            StartCoroutine(Poursuivre(directionJoueur));
        } else if(rand < chancesRoder)
        {
            StartCoroutine(Roder());
        } else
        {
            StartCoroutine(Attendre());
        }
    }

    void Repousse(Vector2 directionJoueur)
    {
        
        directionMouvement = -directionJoueur;
        movementSpeed = movementSpeed * 2f;
        if (etatActuel != Etat.freeze)
        {
            etatActuel = Etat.traitement;
            StartCoroutine(waitForRepousser());
        }
    }

    //On est repoussé par le joueur
    IEnumerator waitForRepousser()
    {
        yield return new WaitForSeconds(0.25f);
        movementSpeed = movementSpeed / 2f;
        if (etatActuel != Etat.freeze && etatActuel != Etat.attaquer)
        {
            etatActuel = Etat.deplacement;
        }
    }

    //On poursuit l'ennemi pendant 2 secondes
    IEnumerator Poursuivre(Vector2 directionJoueur)
    {
        directionMouvement = directionJoueur;
        etatActuel = Etat.traitement;
        yield return new WaitForSeconds(2.0f);
        if (etatActuel != Etat.freeze && etatActuel != Etat.attaquer)
        {
            etatActuel = Etat.deplacement;
        }
    }

    //Attendre arrête tout bonnement les déplacements de l'ennemi pendant 0.5 secondes
    IEnumerator Attendre()
    {
        directionMouvement = new Vector2(0, 0);
        etatActuel = Etat.traitement;
        yield return new WaitForSeconds(0.5f);
        if (etatActuel != Etat.freeze && etatActuel != Etat.attaquer)
        {
            etatActuel = Etat.deplacement;
        }
    }

    //Roder donne une direction aléatoire à l'ennemi pendant 2 secondes
    IEnumerator Roder()
    {
        Vector2 directionRoder = new Vector2(Random.Range(-100, 100), Random.Range(-100, 100));
        directionRoder.Normalize();
        directionMouvement = directionRoder;
        etatActuel = Etat.traitement;
        yield return new WaitForSeconds(2.0f);
        if (etatActuel != Etat.freeze && etatActuel != Etat.attaquer)
        {
            etatActuel = Etat.deplacement;
        }
    }

    void Attaquer(Vector2 directionJoueur)
    {

        directionMouvement = new Vector2(0, 0);

        //On fait en sorte que l'ennemi change de côté si il ne fait pas face au joueur en attaquant
        if (directionJoueur.x < 0 && m_FacingRight)
        {
            Flip();
        }
        else if (directionJoueur.x > 0 && !m_FacingRight)
        {
            Flip();
        }

        //Si l'ennemi est à portée de donner un coup on reste en état attaquer
        //Sinon on déclenche l'état poursuite
        if (Mathf.Abs(directionJoueur.x) < attackDistance && Mathf.Abs(directionJoueur.y) < 0.7 && etatActuel != Etat.freeze)
        {
            etatActuel = Etat.attaquer;
            GetComponent<Animator>().SetTrigger("Attack");
        }

    }

    //Lorsqu'on enclenche le mode berserk, on arrête totalement les ennemis dans le temps
    void Freeze()
    {
        //On desactive les colliders autre au le box collider qui permet la détection lorsque le joueur passe au travers
        
        gameObject.GetComponent<CircleCollider2D>().enabled = false;
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        feet.GetComponent<BoxCollider2D>().enabled = false;
        directionMouvement = Vector3.zero;
    }

    //On 
    public void Unfreeze()
    {
        gameObject.GetComponent<CircleCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        feet.GetComponent<BoxCollider2D>().enabled = true;
        etatActuel = Etat.deplacement;
    }

    void moveEnnemi(Vector2 direction)
    {
        //On fait en sorte que l'ennemi change de côté
        if (direction.x < 0 && m_FacingRight) 
        {
            Flip();
        }
        else if (direction.x > 0 && !m_FacingRight)
        {
            Flip();
        }

        rb.MovePosition((Vector2)transform.position + (direction * movementSpeed * Time.deltaTime));
    }

    //On change le sens de l'ennemi
    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void setDirectionMouvement(Vector2 directionM)
    {
        directionMouvement = directionM;   
    }

    public Vector2 getDirectionMouvement()
    {
        return directionMouvement;
    }

    public void setEtatActuel(int nbEtat)
    {
        switch (nbEtat)
        {
            case 1:
                etatActuel = Etat.deplacement;
                break;
            case 2:
                etatActuel = Etat.attaquer;
                break;
            case 3:
                etatActuel = Etat.traitement;
                break;
            case 4:
                etatActuel = Etat.freeze;
                break;
            case 5:
                etatActuel = Etat.repousse;
                break;
            default:
                break;
        }
    }

    //----------------------------------Gestion des sons-----------------------------------------------------------

    public void TriggerEnnemySound()
    {
        SoundManager.Instance.GetEnnemySound();
    }

    public void TriggerSliceSound()
    {
        SoundManager.Instance.GetBladeHitSound();
    }



    //----------------------------------Gestion de l'attaque de l'ennemi-------------------------------------------


    //Setter de la variable pour savoir si le joueur a bloqué ou non
    public void SetPlayerBlocked(bool isIt)
    {
        playerBlocked = isIt;
    }

    //Setter de la variable pour savoir si l'attaque a touché le joueur
    public void SetAttacksCollideWithPlayer(bool isIt)
    {
        attackCollidesWithPlayer = isIt;
    }

    
    public void StartAttackCoroutine()
    {
        StartCoroutine(AttackCoroutine());
    }

    public IEnumerator AttackCoroutine()
    {
        SetPlayerBlocked(false);

        //On set la durée de l'animation d'attaque
        float duration = 1f;
        float normalizedTime = 0;

        //Tant que l'animation n'est pas terminée et que le joueur n'a pas bloquée, on continue
        while (normalizedTime <= 1f && !playerBlocked)
        {
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }

        //Si l'animation s'est terminée et que l'attaque touche le joueur, il prend des dégats
        if (normalizedTime >= 0.5f && attackCollidesWithPlayer)
        {
            PlayerController.Instance.PlayerGetsHit();
            
        }

        //Si le joueur a bloqué avant la fin de l'animation d'attaque
        else if (normalizedTime < 1f && playerBlocked)
        {
            //Si il a bloqué avant la moitié de l'animation, il fait une garde parfaite
            if (normalizedTime >= 0.08f && normalizedTime <= 0.5f && etatActuel != Etat.freeze)
            {
                Debug.Log(normalizedTime);
                PlayerController.Instance.PlayerPerfectlyBlocked(transform.position);
                directionJoueur = player.transform.position - transform.position;
            }
            //Si il a bloqué après la moitié de l'animation, il fait une garde simple
            else
            {
                PlayerController.Instance.PlayerBlocked(transform.position);
            }
        }
        SetPlayerBlocked(false);
        SetAttacksCollideWithPlayer(false);
        isAttacking = false;
        if (etatActuel != Etat.freeze)
        {
            etatActuel = Etat.deplacement;
        }
        isAgressive = false;
        
    }

    public void SelfDestroy()
    {
        Destroy(gameObject);
    }

    public void DisableTarget()
    {
        target.SetActive(false);
    }
    public void CameraScreenShake()
    {
        CameraShake.Instance.ShakeGlobalCamera(0.1f, 1.5f);
    }

}
