﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemiHitBoxChecker : MonoBehaviour
{
    public EnnemisController parentEnnemi;


    void OnTriggerStay2D(Collider2D other)
    {
        //Si l'ennemi est proche d'un autre, il garde une certaine distance
        /*if (other.tag == "EnnemiHitBox")
        {
            Vector2 limiteProximite = transform.position - other.transform.position;
            if (Mathf.Abs(limiteProximite.x) < 1 && Mathf.Abs(limiteProximite.y) < 1)
            {
                Vector2 directionMouvement = parentEnnemi.getDirectionMouvement();
                directionMouvement = -directionMouvement;
                parentEnnemi.setDirectionMouvement(directionMouvement);
            }

        }*/
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Si l'ennemi est repoussé par la parade parfaite
        if (other.tag == "PushObject")
        {
            parentEnnemi.setDirectionMouvement(new Vector2(0, 0));
            parentEnnemi.setEtatActuel(5);
        }

        /*if(other.tag == "Limite")
        {
            parentEnnemi.setDirectionMouvement(-parentEnnemi.getDirectionMouvement());
            //Debug.Log("Boum !");
        }*/ 
    }
}
