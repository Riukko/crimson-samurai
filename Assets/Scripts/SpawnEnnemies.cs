﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnnemies : MonoBehaviour
{

    public GameObject ennemyPrefab;
    public GameObject spawnZone;
    public GameObject gamePlane;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.Instance.ennemyList.Count < GameManager.Instance.maxEnnemyOnScreen && !PlayerController.Instance.isFrozen && !PlayerController.Instance.isBerserk)
        {
            //Random zone autour du point de spawn
            Vector3 randomPos = Random.insideUnitSphere * 2.5f;
            randomPos.z = 0;
            //On fait spawn l'ennemi dans cette zone
            Instantiate(ennemyPrefab, spawnZone.transform.position + randomPos, Quaternion.identity, gamePlane.transform);
        }
    }
}
