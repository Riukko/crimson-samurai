﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{

    private static UIManager _instance;
    public static UIManager Instance { get => _instance; }

    public GameObject BlockingUIAnimPrefab;

    public Image grayscaleCanvas;

    public GameObject HUD;
    public GameObject menuPause;
    public GameObject menuEnd;
    public Sprite vieVide;

    public TextMeshProUGUI timerText;

    void Awake()
    {

        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this.gameObject);


        }
        else
        {
            Destroy(this);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        grayscaleCanvas.material.SetFloat("_Alpha", 0f);
        HUD.transform.Find("AtakanaUI").Find("Jauge").gameObject.GetComponent<Slider>().maxValue = PlayerController.Instance.maxBerserkGauge;
    }

    void Update()
    {
        //On met à jour le timer
        timerText.text = Mathf.Floor(GameManager.Instance.gameTimer).ToString();

        if (!PlayerController.Instance.isBerserk)
        {
            //On met à jour la jauge de Berserk
            HUD.transform.Find("AtakanaUI").Find("Jauge").gameObject.GetComponent<Slider>().value = PlayerController.Instance.berserkGauge;
        }

        //On met à jour les points de vie
        for(int i=4; i>=PlayerController.Instance.playerHealth;i--)
        {
            if(i>=0)
            {
                HUD.transform.Find("LifeUI").Find("Lifes").GetChild(i).GetComponent<Image>().sprite = vieVide;
            }
        }

        //On met à jour le score
        HUD.transform.Find("KillCount").Find("KillCountText").GetComponent<TextMeshProUGUI>().text = GameManager.Instance.killedDemons.ToString();

        //On check l'input de pause
        if(Input.GetKeyDown(KeyCode.Escape) && !GameManager.Instance.endGame)
        {
            if (GameManager.Instance.gameIsPaused)
            {
                Resume();
            } else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        PlayerController.Instance.Unfreeze();
        GameManager.Instance.gameIsPaused = false;
        menuPause.transform.Find("BoutonMenu").GetComponent<Button>().Select();
        menuPause.SetActive(false);
    }

    public void lerpJaugeBerserk()
    {

        DOTween.To(() => HUD.transform.Find("AtakanaUI").Find("Jauge").gameObject.GetComponent<Slider>().value, x => HUD.transform.Find("AtakanaUI").Find("Jauge").gameObject.GetComponent<Slider>().value = x, 0, 5f) ;
    }

    public void Pause()
    {
        PlayerController.Instance.Freeze();
        GameManager.Instance.gameIsPaused = true;
        menuPause.SetActive(true);
        menuPause.transform.Find("BoutonReprendre").GetComponent<Button>().Select();
    }

    public void EndGame(bool GameOver)
    {
        SoundManager.Instance.StopMusic();
        if (GameOver)
        {
            SoundManager.Instance.GetGameOver();
        }
        PlayerController.Instance.Freeze();
        GameManager.Instance.gameIsPaused = true;
        menuEnd.SetActive(true);
        menuEnd.transform.Find("BoutonMenu").GetComponent<Button>().Select();
        menuEnd.transform.Find("ScoreText").GetComponent<TextMeshProUGUI>().text = GameManager.Instance.killedDemons.ToString();
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }

    public void Restart()
    {
        Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
    }

    public void StartPerfectBlockingAnim(Vector3 spawnPosition)
    {
        GameObject UIAnim = Instantiate(BlockingUIAnimPrefab);
        UIAnim.transform.position = spawnPosition;
        UIAnim.GetComponent<Animator>().SetTrigger("Perfect block");
    }

    public void StartBlockingAnim(Vector3 spawnPosition)
    {
        GameObject UIAnim = Instantiate(BlockingUIAnimPrefab);
        UIAnim.transform.position = spawnPosition;
        UIAnim.GetComponent<Animator>().SetTrigger("Block");
    }

    public void LerpGrayscale(bool appearing)
    {
        grayscaleCanvas.material.DOFloat(appearing ? 1f : 0f, "_Alpha", 3f).Play();
    }

}
