﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager _instance;
    public static SoundManager Instance { get => _instance; }

    public List<AudioClip> ennemiesSounds;
    public List<AudioClip> bladeHitSounds;
    public List<AudioClip> markEnnemiesSounds;
    
    public AudioClip epeeRengainee;
    public AudioClip epeeDegainee;
    public AudioClip epeeFinRengainage;
    public AudioClip walk;
    public AudioClip run;
    public AudioClip paradePerfect;
    public AudioClip parade;
    public AudioClip gameOver;
    public AudioClip ambiantMusic;

    public AudioSource musicSource;
    public AudioSource fxSource;

    public int ennemiesSoundsSize;
    public int bladeHitSoundsSize;
    public int markEnnemiesSoundsSize;

    public float musicRatio;
    public float fxRatio;
    public float effectVolume = 100f;
    public float EffectVolume
    {
        get { return effectVolume; }
        set { effectVolume = value >= 0f && value <= 100f ? value : 100f; }
    }

    public float musicVolume = 100f;
    public float MusicVolume
    {
        get { return musicVolume; }
        set
        {
            musicVolume = value >= 0f && value <= 100f ? value : 100f;
            musicSource.volume = musicVolume * 0.01f * musicRatio;
        }
    }

    void Awake()
    {

        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this.gameObject);


        }
        else
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        ennemiesSoundsSize = ennemiesSounds.Count;
        bladeHitSoundsSize = bladeHitSounds.Count;
        markEnnemiesSoundsSize = markEnnemiesSounds.Count;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetEnnemySound()
    {
        if(ennemiesSoundsSize != 0)
        {
            fxSource.PlayOneShot(ennemiesSounds[Random.Range(0, ennemiesSoundsSize)], effectVolume * 0.01f * fxRatio);
        }
    }

    public void GetBladeHitSound()
    {
        if (bladeHitSoundsSize != 0)
        {
            fxSource.PlayOneShot(bladeHitSounds[Random.Range(0, bladeHitSoundsSize)], effectVolume * 0.01f * fxRatio);
        }
    }

    public void GetMarkEnnemySound()
    {
        if (markEnnemiesSoundsSize != 0)
        {
            fxSource.PlayOneShot(markEnnemiesSounds[Random.Range(0, markEnnemiesSoundsSize)], effectVolume * 0.01f * fxRatio);
        }
    }

    public void GetWalkSound()
    {
        fxSource.PlayOneShot(walk, effectVolume * 0.01f * fxRatio);
    }

    public void GetRunSound()
    {
        fxSource.PlayOneShot(run, effectVolume * 0.01f * fxRatio);
    }

    public void GetEpeeRengainee()
    {
        fxSource.PlayOneShot(epeeRengainee, effectVolume * 0.01f * fxRatio);
    }

    public void GetEpeeFinRengainage()
    {
        fxSource.PlayOneShot(epeeFinRengainage, effectVolume * 0.05f * fxRatio);
    }

    public void GetEpeeDegainee()
    {
        fxSource.PlayOneShot(epeeDegainee, effectVolume * 0.01f * fxRatio);
    }

    public void GetParade()
    {
        fxSource.PlayOneShot(parade, effectVolume * 0.01f * fxRatio);
    }
    
    public void GetParadePerfect()
    {
        fxSource.PlayOneShot(paradePerfect, effectVolume * 0.01f * fxRatio);
    }

    public void GetGameOver()
    {
        musicSource.PlayOneShot(gameOver, musicVolume * 0.01f * musicRatio);
    }

    public void GetAmbiantMusic()
    {
        musicSource.PlayOneShot(ambiantMusic, musicVolume * 0.01f * musicRatio);
    }

    public void StopMusic()
    {
        musicSource.Stop();
    }
}
