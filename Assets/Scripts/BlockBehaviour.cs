﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //On vérifie que le joueur bloque bien une attaque, si oui, on passe la variable playerBlocked de l'ennemi à true
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Threats"))
        {
            
            collision.transform.GetComponentInParent<EnnemisController>().SetPlayerBlocked(true);
        }
        else
        {

        }

        
    }
}
