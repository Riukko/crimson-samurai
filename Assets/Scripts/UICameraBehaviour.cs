﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICameraBehaviour : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        GetComponent<Camera>().orthographicSize = transform.parent.GetComponent<Camera>().orthographicSize;
    }
}
