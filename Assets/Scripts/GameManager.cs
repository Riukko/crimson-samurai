﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using Cinemachine;
public class GameManager : MonoBehaviour
{
    public GameObject playerCamera;
    public GameObject globalCamera;

    Cinemachine.CinemachineImpulseSource source;

    public GameObject player;
    public bool endGame = false;

    public float timerBerserkMode = 5f;
    public List<GameObject> ennemyList;
    public HashSet<GameObject> killedEnemyList;
    public int maxEnnemyOnScreen = 5;

    private static GameManager _instance;
    public static GameManager Instance { get => _instance;  }
    //ajout Paul
    //Cette variable va servir à limiter le nombre d'ennemis aggressifs (voir EnnemisController)
    public float nbEnnemisAgressifs = 0;
    public float maxEnnemisAgressifs = 2;

    public float killedDemons = 0;
    public bool gameIsPaused = false;
    public float gameTimer;
    public bool timeFreeze = false;

    public List<ParticleSystem> sakuraParticleList;

    void Awake()
    {

        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this.gameObject);


        }
        else
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        SoundManager.Instance.GetAmbiantMusic();
        killedEnemyList = new HashSet<GameObject>();
        gameTimer = 120f;
    }
        


    // Update is called once per frame
    void Update()
    {

        //ajout Paul
        float nbAgressifs = 0;
        foreach(GameObject ennemi in ennemyList)
        {
            if(ennemi.GetComponent<EnnemisController>().isAgressive)
            {
                nbAgressifs++;
            }
        }

        nbEnnemisAgressifs = nbAgressifs;

        if(gameIsPaused)
        {
            Time.timeScale = 0f;
        } else
        {
            Time.timeScale = 1f;
        }

        if (!timeFreeze && gameTimer > 0)
        {
            gameTimer -= Time.deltaTime;
        }
        else if(gameTimer <= 0)
        {
            UIManager.Instance.EndGame(false);
        }
    }
    



    void CameraSwitchToGlobal(bool enableGrayScale)
    {
        playerCamera.SetActive(false);
        globalCamera.SetActive(true);
        if (enableGrayScale)
        {
            DOTween.To(() => SoundManager.Instance.musicSource.volume, x => SoundManager.Instance.musicSource.volume = x, 0f, 3f);
            UIManager.Instance.LerpGrayscale(true);
        }
    }

    void CameraSwitchToPlayer(bool enableGrayScale)
    {
        playerCamera.SetActive(true);
        globalCamera.SetActive(false);
        if (enableGrayScale)
        {
            DOTween.To(() => SoundManager.Instance.musicSource.volume, x => SoundManager.Instance.musicSource.volume = x, 1f, 3f);
            UIManager.Instance.LerpGrayscale(false);
        }
    }

    //---------------------------------------------------SON------------------------------------------


    public static float getNbEnnemisAgressifs()
    {
        return _instance.nbEnnemisAgressifs;
    }

    public static float getMaxEnnemisAgressifs()
    {
        return _instance.maxEnnemisAgressifs;
    }

    public IEnumerator TriggerBerserkMode(bool isItEnd)
    {
        //On bloque les mouvements du joueur le temps de l'animation
        PlayerController.Instance.isFrozen = true;

        //On reset la jauge de berserk
        PlayerController.Instance.berserkGauge = 0;

        //

        //La vitesse du joueur est augmentée
        PlayerController.Instance.runSpeed = isItEnd ? 110f : 90f;

        //On passe le joueur en mode berserk
        PlayerController.Instance.setIsBerserk(true);
        PlayerController.Instance.GetComponent<Animator>().SetBool("berserkMode", true);

        //On incrémente de deux le nombre d'ennemis possible à l'écran
        maxEnnemyOnScreen += 2;

        //On passe en caméra globale
        CameraSwitchToGlobal(true);

        //On active le trail du joueur au niveau du sabre
        PlayerController.Instance.SetActiveTrail(true);

        //On pause les particules des fleurs de cerisiers
        for (int i = 0; i < sakuraParticleList.Count; i++)
        {
            sakuraParticleList[i].Pause();
        }

        //On freeze tous les ennemis
        for (int i = 0; i < ennemyList.Count; i++)
        {
            ennemyList[i].GetComponent<EnnemisController>().etatActuel = EnnemisController.Etat.freeze;
            //On arrête leur animation si ils sont en train d'en faire une
            ennemyList[i].GetComponent<Animator>().Rebind();
        }

        yield return new WaitForSeconds(2f);

        //Animation dégainage
        PlayerController.Instance.gameObject.GetComponent<Animator>().SetTrigger("Unsheath");
        
        //On attent que l'animation se termine
        yield return new WaitForSeconds(3f);

        //On repasse en caméra du joueur
        CameraSwitchToPlayer(false);

        //On débloque le joueur
        PlayerController.Instance.isFrozen = false;

        //On lance le timer
        float duration = timerBerserkMode;

        UIManager.Instance.lerpJaugeBerserk();

        while (duration > 0)
        {
            duration -= Time.deltaTime;
            yield return null;
        }

        //On vérifie que des ennemis ont été tués
        if(killedEnemyList.Count != 0 && duration <= 0)
        {
            //On bloque les mouvements du joueur le temps de l'animation
            PlayerController.Instance.isFrozen = true;

            //On arrête le timer
            timeFreeze = true;

            //On desactive le mode berserk du joueur
            PlayerController.Instance.setIsBerserk(false);
            PlayerController.Instance.gameObject.GetComponent<Animator>().SetBool("berserkMode", false);

            //On repasse en caméra globale
            CameraSwitchToGlobal(false);
            yield return new WaitForSeconds(2f);

            //On remet la vitesse du joueur à la normale
            PlayerController.Instance.runSpeed = 40f;

            //Animation de rengainage ou mort selon si c'est la fin
            if (!isItEnd)
            {
                PlayerController.Instance.gameObject.GetComponent<Animator>().SetTrigger("Sheath");
            }
            else
            {
                PlayerController.Instance.gameObject.GetComponent<Animator>().SetTrigger("Death");
            }

            //On attend la fin de l'animation
            yield return new WaitForSeconds( 2f);

            //On relance les particules des fleurs de cerisiers
            for (int i = 0; i < sakuraParticleList.Count; i++)
            {
                sakuraParticleList[i].Play();
            }

            //On check la liste des ennemies tués
            foreach (GameObject ennemy in killedEnemyList)
            { 

                //On trigger l'animation de mort
                ennemy.GetComponent<Animator>().SetTrigger("Death");

                //On incrémente le nombre de démons tué
                killedDemons++;

                //On enlève l'ennemi tué de la liste des ennemis de la scène
                ennemyList.Remove(ennemy);

                //Chaque animation est séparée par un petit laps de temps
                yield return new WaitForSeconds(0.2f);
            }
            yield return new WaitForSeconds(0.5f);
            //On reset la liste des ennemis tués
            killedEnemyList = new HashSet<GameObject>();
            //On unfreeze tous les ennemis
            for (int i = 0; i < ennemyList.Count;  i++)
            {
                ennemyList[i].GetComponent<EnnemisController>().Unfreeze();
            }

            //On fait disparaitre la trail
            PlayerController.Instance.SetActiveTrail(false);



            if (!isItEnd)
            {
                //On repassse en caméra du joueur
                CameraSwitchToPlayer(true);
                yield return new WaitForSeconds(1.5f);
                //On relance le temps
                timeFreeze = false;

                //On débloque le joueur
                PlayerController.Instance.isFrozen = false;
            }
            else
            {
                CameraSwitchToPlayer(true);
                yield return new WaitForSeconds(2f);

                //On lance le menu de fin
                UIManager.Instance.EndGame(true);
            }

        }

        //Si le joueur n'a tué aucun ennemi
        else
        {
            //On desactive le mode berserk du joueur
            PlayerController.Instance.setIsBerserk(false);
            PlayerController.Instance.gameObject.GetComponent<Animator>().SetBool("berserkMode", false);

            //On passe en caméra globale
            CameraSwitchToGlobal(false);
            yield return new WaitForSeconds(1.5f);
            //On remet la vitesse du joueur à la normale
            PlayerController.Instance.runSpeed = 40f;
            //On freeze le joueur le temps de l'animation de caméra
            PlayerController.Instance.isFrozen = true;
            yield return new WaitForSeconds(2f);

            //On unfreeze les ennemis
            for (int i = 0; i < ennemyList.Count; i++)
            {
                ennemyList[i].GetComponent<EnnemisController>().Unfreeze();
            }

            //On relance les particules des fleurs de cerisiers
            for (int i = 0; i < sakuraParticleList.Count; i++)
            {
                sakuraParticleList[i].Play();
            }

            //On désactive la trail
            PlayerController.Instance.SetActiveTrail(false);

            if (!isItEnd)
            {
                //On repassse en caméra du joueur
                CameraSwitchToPlayer(true);
                yield return new WaitForSeconds(1.5f);
                //On relance le temps
                timeFreeze = false;

                //On débloque le joueur
                PlayerController.Instance.isFrozen = false;
            }
            else
            {
                CameraSwitchToPlayer(true);
                yield return new WaitForSeconds(2f);
                //On lance le menu de fin
                UIManager.Instance.EndGame(true);
            }
        }


    }



}
