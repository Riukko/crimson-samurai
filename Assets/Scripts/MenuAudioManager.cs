﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAudioManager : MonoBehaviour
{
    public AudioSource audioManager;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Play()
    {
        audioManager.time = 1.2f;
        audioManager.Play();
    }
}
