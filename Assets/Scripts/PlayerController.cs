﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    //Singleton
    private static PlayerController _instance;
    public static PlayerController Instance { get => _instance; }



    public float runSpeed = 40f;

    float horizontalMove = 0f;
    float verticalMove = 0f;

    public int playerHealth = 5;
    public int berserkGauge = 0;
    public int maxBerserkGauge = 10;

    public bool isFrozen = false;
    public bool isInvicible = false;
    private bool cooldownBlock = false;
    public bool isBerserk;

    public CharacterController2D controller;
    public GameObject perfectParadePush;
    public TrailRenderer berserkTrail;

    void Awake()
    {

        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this.gameObject);


        }
        else
        {
            Destroy(this);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        if (!isFrozen)
        {
            horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
            verticalMove = Input.GetAxisRaw("Vertical") * runSpeed;

            if (Input.GetButtonDown("Block") && !cooldownBlock && !isBerserk)
            {
                //Pour lancer le knockback à chaque parade
                //Instantiate(perfectParadePush, transform.position, Quaternion.identity);
                Block();
                StartCoroutine(CooldownBlock());
            }

        }
        else
        {
            horizontalMove = 0f;
            verticalMove = 0f;
        }
        GetComponent<Animator>().SetFloat("Speed", Mathf.Abs(horizontalMove) + Mathf.Abs(verticalMove));
    }

    private void FixedUpdate()
    { 
        controller.HorizontalMove(horizontalMove * Time.fixedDeltaTime);
        controller.VerticalMove(verticalMove * Time.fixedDeltaTime);
        
    }

    //---------------------------------------------------SON------------------------------------------
    private void TriggerWalkSound()
    {
        SoundManager.Instance.GetWalkSound();
    }

    public void TriggerUnsheathingSound()
    {
        SoundManager.Instance.GetEpeeDegainee();
    }

    public void TriggerSheathingSound()
    {
        SoundManager.Instance.GetEpeeRengainee();
    }

    public void TriggerEndOfSheathing()
    {
        SoundManager.Instance.GetEpeeFinRengainage();
    }
    //---------------------------------------------------------------------------------------------
    public void Block(bool animationEnded=false)
    {
        isFrozen = true;
        GetComponent<Animator>().SetTrigger("Block");
    }

    public void Unfreeze()
    {
        isFrozen = false;
    }

    public void Freeze()
    {
        isFrozen = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Threats"))
        {
            collision.transform.parent.gameObject.GetComponent<EnnemisController>().SetAttacksCollideWithPlayer(true);
        }
    }

    

    public void PlayerGetsHit()
    {
        if (!isInvicible)
        {
            playerHealth--;
            if(playerHealth<0)
            {
                playerHealth = 0;
            }
            if (playerHealth > 0)
            {
                isInvicible = true;
                GetComponent<Animator>().SetBool("Invincible", true);
                GetComponent<Animator>().SetTrigger("Hit");
                DOVirtual.DelayedCall(3f, () =>
                {
                    isInvicible = false;
                    GetComponent<Animator>().SetBool("Invincible", false);
                });
            }
            else
            {
                GameManager.Instance.endGame = true;
                StartCoroutine(GameManager.Instance.TriggerBerserkMode(true));
            }
        }
    }
    
    public void PlayerPerfectlyBlocked(Vector3 positionEnnemy)
    {
        UIManager.Instance.StartPerfectBlockingAnim(transform.position - positionEnnemy / 4);
        berserkGauge += 2;
        //Si la parade est parfaite, on reset le cooldown
        cooldownBlock = false;
        StopCoroutine("CooldownBlock");
        //Si la parade est parfaite, on repousse les ennemis autour
        Instantiate(perfectParadePush, transform.position, Quaternion.identity);
        CameraShake.Instance.ShakePlayerCamera(0.1f, 2f);
        SoundManager.Instance.GetParadePerfect();
        CheckIfBerserkGaugeFilled();

    }

    public void PlayerBlocked(Vector3 positionEnnemy)
    {
        UIManager.Instance.StartBlockingAnim(transform.position - positionEnnemy / 4);
        berserkGauge += 1;
        StartCoroutine(CooldownBlock());
        SoundManager.Instance.GetParade();
        CheckIfBerserkGaugeFilled();
    }

    IEnumerator CooldownBlock()
    {
        cooldownBlock = true;
        yield return new WaitForSeconds(2f);
        cooldownBlock = false;
    }

    public void SetActiveTrail(bool isActive)
    {
        berserkTrail.emitting = isActive;
    }

    public void CheckIfBerserkGaugeFilled()
    {
        if(berserkGauge >= maxBerserkGauge)
        {
            StartCoroutine(GameManager.Instance.TriggerBerserkMode(false));
        }
    }

    public void setIsBerserk(bool isItBerserk)
    {
        isBerserk = isItBerserk;
    }

}
