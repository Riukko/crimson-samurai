﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;

public class CameraShake : MonoBehaviour
{

    public float ShakeDuration = 0.3f;          // Time the Camera Shake effect will last
    public float ShakeAmplitude = 1.2f;         // Cinemachine Noise Profile Parameter
    public float ShakeFrequency = 2.0f;         // Cinemachine Noise Profile Parameter

    private float PlayerCameraShakeElapsedTime = 0f;
    private float GlobalCameraShakeElapsedTime = 0f;

    // Cinemachine Shake
    public CinemachineVirtualCamera playerVirtualCamera;
    public CinemachineVirtualCamera globalVirtualCamera;
    private CinemachineBasicMultiChannelPerlin playerVirtualCameraNoise;
    private CinemachineBasicMultiChannelPerlin globalVirtualCameraNoise;

    public static CameraShake _instance;
    public static CameraShake Instance{get =>  _instance; }
    void Awake()
    {

        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this.gameObject);


        }
        else
        {
            Destroy(this);
        }
    }

    // Use this for initialization
    void Start()
    {
        // Get Virtual Camera Noise Profile
        if (playerVirtualCamera != null)
        {
            playerVirtualCameraNoise = playerVirtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        }

        globalVirtualCameraNoise = globalVirtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();


    }

    // Update is called once per frame
    void Update()
    {
        // TODO: Replace with your trigger
        //if (Input.GetKey(KeyCode.C))
        //{
        //    ShakePlayerCamera(0.3f, 1.5f);
        //}
        //if (Input.GetKey(KeyCode.V))
        //{
        //    ShakeGlobalCamera(0.3f,1.5f);
        //}


        // If the Cinemachine componet is not set, avoid update
        if (playerVirtualCamera != null && playerVirtualCameraNoise != null)
        {
            // If Camera Shake effect is still playing
            if (PlayerCameraShakeElapsedTime > 0)
            {
                // Set Cinemachine Camera Noise parameters
                playerVirtualCameraNoise.m_AmplitudeGain = ShakeAmplitude;
                playerVirtualCameraNoise.m_FrequencyGain = ShakeFrequency;

                // Update Shake Timer
                PlayerCameraShakeElapsedTime -= Time.deltaTime;
            }
            else if(GlobalCameraShakeElapsedTime > 0)
            {
                // Set Cinemachine Camera Noise parameters
                globalVirtualCameraNoise.m_AmplitudeGain = ShakeAmplitude;
                globalVirtualCameraNoise.m_FrequencyGain = ShakeFrequency;

                // Update Shake Timer
                GlobalCameraShakeElapsedTime -= Time.deltaTime;
            }

            else
            {
                // If Camera Shake effect is over, reset variables
                globalVirtualCameraNoise.m_AmplitudeGain = 0f;
                playerVirtualCameraNoise.m_AmplitudeGain = 0f;
                GlobalCameraShakeElapsedTime = 0f;
                PlayerCameraShakeElapsedTime = 0f;
            }
        }
    }

    public void ShakePlayerCamera(float duration, float amplitude)
    {
        PlayerCameraShakeElapsedTime = duration;
        ShakeAmplitude = amplitude;
    }

    public void ShakeGlobalCamera(float duration, float amplitude)
    {
        GlobalCameraShakeElapsedTime = duration;
        ShakeAmplitude = amplitude;
    }
}

